provider "azurerm" {
  version = "=1.38.0"
}
variable "prefix" {
  default = "tfvmex"
}

# resource "azurerm_resource_group" "nisumnv3" {
#   name     = "Grupo_3_test"
#   location = "West US 2"
# }

resource "azurerm_kubernetes_cluster" "example" {
  name                = "example-aks2"
  location            = "West US 2"
  resource_group_name = "Grupo_1_test"
  dns_prefix          = "exampleaks2"

  default_node_pool {
    name       = "default"
    node_count = 1
    vm_size    = "Standard_D2_v2"
  }

  service_principal {
    client_id     = "e00671ac-1dc3-49cc-8db4-d3f5158c3e9d"
    client_secret = "d07a5a7c-24bd-4dd2-bb48-e4e02e7929c7"
  }

  tags = {
    Environment = "Production"
  }
}


